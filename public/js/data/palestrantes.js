var palestrantes = {
  alexos: {
    nome: "Alexandro Silva a.k.a Alexos",
    trilha: "trilha-seguranca",
    titulo: "Usando Ansible para Orquestração de Segurança e Conformidade",
    minibio: "Alexandro Silva atua a mais de 10 anos como especialista em segurança de redes e sistemas Unix/Linux. Atualmente é sócio-CTO da iBLISS Digital Security possui artigo publicados na BSDMag, H2HC Magazine, além de ser co-fundador da Nullbyte Security Conference e palestrante em eventos como Latinoware, FISL, Security BSides, BHack, Vale Security Conference.",
    resumo: "Hardening de servidores e serviços é uma premissa básica para segurança e conformidade com diversos padrões (e.g PCI-DSS). <br> Executar está tarefa em centenas ou milhares de servidores não é uma tarefa fácil. <br> Nesta palestra irei demonstrar como é possível automatizar todo o processo de orquestração de infraestrutura e implantação de baseline usando o Ansible."
  },

  aurium: {
    nome: "Aurélio Adnauer Heckert",
    trilha: "trilha-impactos",
    titulo: "Como (possivelmente) seus filhos e (certamente) seus netos serão escravizados",
    minibio: "Desenvolvedor e usuário de SL. Participou do time de desenvolvimento do Inkscape e colabora com vários outros projetos livres. É co-fundador do PSL-BA da Colivre.",
    resumo: "Nossa dependência do ciberespaço é crescente numa ordem inversa à nossa compreensão sobre a sua natureza e seus impactos políticos. Mas nada do que vemos hoje está perto das consequências do transumanismo para as futuras gerações. A ficção científica nos chama atenção para muitos riscos, mas nunca falou sobre corpos privatizados por corporações... Chegaremos lá por nossa escolha e não vai demorar"
  },

  geisa: {
    nome: "Geisa Santos",
    trilha: "trilha-impactos",
    titulo: "Autonomia tecnológica para equidades de gênero, raça e classe",
    minibio: "",
    resumo: "Para conversar sobre democratização do conhecimento, software livre, acessos, representantes de grupos atuantes na luta pela diversidade na tecnologia apresentam seus pontos de vista e convidam os ouvintes a participarem do diálogo"
  },

  gildasio: {
    nome: "Gildásio Júnior",
    trilha: "trilha-seguranca",
    titulo: "DNS - O que você precisa saber",
    minibio: "Gerente de Infraestrutura e Segurança da InfoJr UFBA, Bolsista no PoP-BA e CoSIC/UFBA atuando principalmente no tratamento de incidentes de segurança, e participante do Raul Hacker Club. Também estudante do DCC/UFBA.",
    resumo: "A apresentação fala sobre DNS, não abordando na prática, mas deixando explicado vários temas interessantes sobre a tecnologia. Inclusive, falando algumas curiosidades e informações pouco divulgadas e entendidas sobre o tema."
  },

  jprodrigues: {
    nome: "JP Rodrigues",
    trilha: "trilha-impactos",
    titulo: "A sua empresa também é impulsionada pelo Software Livre?",
    minibio: "JP é desenvolvedor web e de aplicativos. HTML, CSS e JavaScript são suas ferramentas favoritas. Sua vida enquanto desenvolvedor é marcada por dois pontos de muito aprendizado: a InfoJr UFBA, e a StartOnApp.",
    resumo: "O uso de software livre como sistema operacional, editor de texto ou até mesmo como um utilitário qualquer do nosso dia-a-dia, todo mundo já deve ter experimentado, mas e se o assunto for “O uso do software livre como ferramenta para sustentar e impulsionar seu negócio”? Essa palestra tem o objetivo de expor e dialogar a respeito da relação da StartOnApp com as tecnologias livres e como isso vem colaborando para o crescimento de uma empresa que nasceu dentro da universidade."
  },

  juliana: {
    nome: "Juliana Oliveira",
    trilha: "trilha-impactos",
    titulo: "Como e por que contribuir com projetos de software livre?",
    minibio: "Graduanda em Sistemas de Informação (UFBA). Participa do Projeto de Pesquisa sobre Negras e Negros na Computação. Membra do Programa Onda Digital e do Projeto Meninas Digitais – Regional Bahia. Atua ministrando cursos de internet e tecnologias abertas para jovens, adultos e idosos e cursos de iniciação à programação de computadores para mulheres. Tem realizado trabalhos principalmente nos seguintes temas: software livre, inclusão digital, inclusão digital na terceira idade, extensão universitária, informática e educação, educação em computação, raça e tecnologia, gênero e tecnologia.",
    resumo: "Liberdade, autonomia, segurança, democratização do conhecimento e sustentabilidade econômica são algumas das motivações para adotar e contribuir em projetos de software livre. Na palestra serão apresentadas possibilidades de contribuição em projetos de software livre, os desafios enfrentados e formas de superar as adversidades, os benefícios para quem contribui, as principais ferramentas utilizadas e alguns projetos de incentivo à contribuição em projetos de software livre e open source. Além disso, serão apresentadas iniciativas que aumentam a visibilidade de mulheres que atuam com tecnologias livres e criam oportunidades de participação feminina. "
  },

  mauricioCesar: {
    nome: "Mauricio Cesar",
    trilha: "trilha-desenvolvimento",
    titulo: "R para Iniciantes",
    minibio: "Consultor de Business Intelligence (BI), Data Discovery e Business Analytics, palestrante, instrutor da Suíte Open Source de Business Intelligence Pentaho, instrutor da plataforma de Business Discovery QlikView e especialista no desenvolvimento de soluções baseadas em análise, integração e migração de dados.",
    resumo: "A linguagem de análise de dados mais utilizada no momento será apresentada através de exercícios e exemplos bem práticos utilizando bases de dados abertas. O objetivo final é apresentar aos participantes na forma de um tutorial os principais recursos dessa linguagem e seu potencial para a análise de dados."
  },

  rafael: {
    nome: "Raf Fael",
    trilha: "trilha-desenvolvimento",
    titulo: " Jupyter Notebook para estudantes de engenharia e ciências",
    minibio: "Estudante de Engenharia Elétrica, Técnólogo em Análise e Desenvolvimento de Sistemas, Técnico Industrial. Já trampou com robótica, automação da manufatura, hidráulica, pneumática, pneutrônica, usinagem com comando numérico computadorizado e hoje trabalha operando unidades industriais de controle ambiental, utilidades e sistemas digitais de controle distribuído em infraestruturas críticas de refino.",
    resumo: "Vamos utilizar a ferramenta Jupyter para realizar a análise e projeto de sistemas de engenharia usando sua linguagem de programação favorita, documentar código com tipografia matemática de alta qualidade, comunicar resultados de maneira clara e permitir a reprodutibilidade de análise de dados e experimentos computacionais de maneira padronizada."
  },

  rogerio: {
    nome: "Rogerio Bastos",
    trilha: "trilha-seguranca",
    titulo: "Firewall - Dicas e Boas Práticas",
    minibio: "",
    resumo: "O firewall é um componente de segurança essencial em toda e qualquer rede e tem como função controlar o tráfego que passar por ele de acordo com as regras definidas pelo administrador. Esta apresentação tem como objetivo mostrar dicas, boas práticas e referências para a criação de políticas e regras de firewall de modo que possam ser aplicadas em diferentes ambientes, independete da soluções de firewall utilizada. Baseada em RFCs, BCPs e outros guias e normas internacionais, o conteúdo contempla as versões v4 e v6 da pilha de protocolos IP e mostra exemplos ilustrativos utilizando a solução de firewall do Linux, por ser uma opção open-source e amplamente utilizada. Dentre os temas abordados, pode-se citar: estratégias para a criação de regras, bloqueio de endereços inválidos, bloqueio de tráfego multicast e broadcast, filtragem de tráfego de entrada para evitar ataque de spoofing, bloqueio de pacotes ICMP perigosos, estratégias e ferramentas para validação do firewall."
  },

};
