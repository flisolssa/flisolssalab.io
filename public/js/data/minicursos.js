var minicursos = {

  danilo: {
    nome: "Danilo Daltro",
    minibio: "Estudante do curso de Ciência da Computação na Universidade Federal da Bahia. Bolsista do projeto Suburmídias e integrante da Equipe de Desenvolvimento do Programa Onda Digital, onde atuo como voluntário desde 2016/Dez. Trabalho com a plataforma Noosfero, pautando a filosofia em software livre, atuando no desenvolvimento de sites para o programa.",
    resumo: "O Noosfero é uma ferramenta livre criada pela cooperativa Colivre, que é formada por ex-alunos da UFBA. Essa tecnologia permite a criação de redes sociais de forma prática, contudo, possibilitando a personalização do ambiente para atender a criação de campos virtuais, intranets sociais e sites em geral.\nPor ser um software livre, o Noosfero torna possível a democratização do acesso à tal tecnologia, sendo assim possibilita que qualquer indivíduo, grupos e empresas criem soluções se apropriando deste software. E por isso o framework tem tido aplicações em diferentes contextos como: campus virtual de universidades, intranet de empresas, objetos de estudo de pesquisas científicas e como redes sociais educacionais. Sendo assim torna-se uma grande oportunidade a disseminação desta plataforma para que toda a comunidade se aproprie desta ferramenta."
  },

  felipe: {
    nome: "Felipe Araujo",
    minibio: "Bacharelando em Ciência da Computação pela Universidade Federal da Bahia - UFBA, tem experiência com desenvolvimento de aplicações web e mobile, é entusiasta do software e outras tecnologias livres, atuando no Grupo de Pesquisa e Extensão Onda Digital (UFBA) desde 2015. Atualmente é voluntário do grupo, onde realiza atividades de desenvolvimento de novas funcionalidades e redefinição do layout da plataforma de ensino e aprendizagem TecCiencia, baseada no software livre Noosfero.",
    resumo: "O Noosfero é uma ferramenta livre criada pela cooperativa Colivre, que é formada por ex-alunos da UFBA. Essa tecnologia permite a criação de redes sociais de forma prática, contudo, possibilitando a personalização do ambiente para atender a criação de campos virtuais, intranets sociais e sites em geral.\nPor ser um software livre, o Noosfero torna possível a democratização do acesso à tal tecnologia, sendo assim possibilita que qualquer indivíduo, grupos e empresas criem soluções se apropriando deste software. E por isso o framework tem tido aplicações em diferentes contextos como: campus virtual de universidades, intranet de empresas, objetos de estudo de pesquisas científicas e como redes sociais educacionais. Sendo assim torna-se uma grande oportunidade a disseminação desta plataforma para que toda a comunidade se aproprie desta ferramenta."
  },

  karen: {
    nome: "Karen Santos",
    minibio: "Graduanda do Bacharelado Interdisciplinar em Ciência e Tecnologia pela Universidade Federal da Bahia (UFBA). Membro do Programa de Pesquisa e Extensão Onda Digital desde 2016, com participação na equipe de desenvolvimento, como desenvolvedora front-end. Atualmente atuando no projeto de pesquisa sobre Design instrucional e de interação para MOOCs.",
    resumo: "O Noosfero é uma ferramenta livre criada pela cooperativa Colivre, que é formada por ex-alunos da UFBA. Essa tecnologia permite a criação de redes sociais de forma prática, contudo, possibilitando a personalização do ambiente para atender a criação de campos virtuais, intranets sociais e sites em geral.\nPor ser um software livre, o Noosfero torna possível a democratização do acesso à tal tecnologia, sendo assim possibilita que qualquer indivíduo, grupos e empresas criem soluções se apropriando deste software. E por isso o framework tem tido aplicações em diferentes contextos como: campus virtual de universidades, intranet de empresas, objetos de estudo de pesquisas científicas e como redes sociais educacionais. Sendo assim torna-se uma grande oportunidade a disseminação desta plataforma para que toda a comunidade se aproprie desta ferramenta."
  },

  luiz: {
    nome: "Luiz Cláudio Silva",
    minibio: "Formado em Processamento de Dados pela Faculdade Rui Barbosa, especialista em Redes de Computadores pela Unifacs. Trabalha com desenvolvimento e administração de sistemas e métodos ágeis desde 1996, tendo usado diferentes linguagens e metodologias. Tem participado de diversos eventos como organizador, instrutor e palestrante. É membro dos grupos Python Bahia e NoSQL-BA e apoia os grupos LinguÁgil e GDG Salvador.",
    resumo: "Já há alguns anos, a linguagem de programação Python vem se destacando entre as mais utilizadas. Tanto por sua facilidade de ser aprendida quanto pela grande quantidade de áreas em que pode ser empregada, Python é uma ótima opção como primeira ou próxima linguagem para estudantes e profissionais, sejam da área de Computação ou não. Neste minicurso, serão apresentadas as caracterísitcas básicas da linguagem Python e propostas atividades simples para ajudar a compreensão. Ao final, serão dadas dicas e sugestões de como continuar aprendendo e usando Python."
  },

  marcos: {
    nome: "Marcos Gomes",
    minibio: "Estudante de Análise e Desenvolvimento de Sistemas, usuário de GNU/Linux desde 2011 e Bacharel em Violão Erudito pela UFBA (2014);",
    resumo: "Minicurso introdutório ao desenvolvimento de jogos com ferramentas livres (incluindo GNU/Linux).\nAlguns projetos pessoais:\n(Color Challenge - https://github.com/GomesMarcos/colorchallenge)\n(Spinner Battle - https://github.com/GomesMarcos/SpinnerBattle)\n(Grupetto - https://play.google.com/store/apps/details?id=org.godotengine.grupetto&hl=pt_BR)\n\nProjetos de Iniciação Científica:\n(NR-SIM - https://play.google.com/store/apps/details?id=com.QuemSabe.NRSIM&hl=pt_BR)\n(Person Defense - https://play.google.com/store/apps/details?id=com.ToBTeam.PersonDefense&hl=pt_BR)\n\nFerramentas a serem abordadas:\nInkscape - Vetorização de Imagens\nLMMS - Trilha e Efeitos Sonoros\nGodot - Motor de Desenvolvimento de Jogos;"
  },

  mauricio: {
    nome: "Mauricio Taffarel Barreto da Silva",
    minibio: "O Noosfero é uma ferramenta livre criada pela cooperativa Colivre, que é formada por ex-alunos da UFBA. Essa tecnologia permite a criação de redes sociais de forma prática, contudo, possibilitando a personalização do ambiente para atender a criação de campos virtuais, intranets sociais e sites em geral.\nPor ser um software livre, o Noosfero torna possível a democratização do acesso à tal tecnologia, sendo assim possibilita que qualquer indivíduo, grupos e empresas criem soluções se apropriando deste software. E por isso o framework tem tido aplicações em diferentes contextos como: campus virtual de universidades, intranet de empresas, objetos de estudo de pesquisas científicas e como redes sociais educacionais. Sendo assim torna-se uma grande oportunidade a disseminação desta plataforma para que toda a comunidade se aproprie desta ferramenta.",
    resumo: "O curso busca introduzir o uso de um software livre na modelagem e animação 3D, hoje fortemente dominada por softwares proprietários, ressaltar a importância crescente dos objetos 3D e dar uma visão geral do Blender."
  },

  murilo: {
    nome: "Murilo Vicente",
    minibio: "Estudante de graduação pela Universidade Federal da Bahia (UFBA) do Bacharelado Interdisciplinar em Ciência e Tecnologia. Membro do Programa de Pesquisa e Extensão Onda Digital desde 2017, atuando na equipe de desenvolvimento. Bolsista do projeto TecCiencia, uma tecnologia social para educação.",
    resumo: "O Noosfero é uma ferramenta livre criada pela cooperativa Colivre, que é formada por ex-alunos da UFBA. Essa tecnologia permite a criação de redes sociais de forma prática, contudo, possibilitando a personalização do ambiente para atender a criação de campos virtuais, intranets sociais e sites em geral.\nPor ser um software livre, o Noosfero torna possível a democratização do acesso à tal tecnologia, sendo assim possibilita que qualquer indivíduo, grupos e empresas criem soluções se apropriando deste software. E por isso o framework tem tido aplicações em diferentes contextos como: campus virtual de universidades, intranet de empresas, objetos de estudo de pesquisas científicas e como redes sociais educacionais. Sendo assim torna-se uma grande oportunidade a disseminação desta plataforma para que toda a comunidade se aproprie desta ferramenta."
  },

  nicolle: {
    nome: "Nicolle Souza Leto",
    minibio: "",
    resumo: "O curso busca introduzir o uso de um software livre na modelagem e animação 3D, hoje fortemente dominada por softwares proprietários, ressaltar a importância crescente dos objetos 3D e dar uma visão geral do Blender.Mauricio Taffarel Barreto da Silva"
  },


};
