var nav = $('#menu');

$(window).scroll(function() {
    if ($(this).scrollTop() > 15) {
        nav.addClass('scrolled');
    } else {
        nav.removeClass('scrolled');
    }
});

$('#bars').click(function() {
    nav.toggleClass('active');
});

var $doc = $('html, body');
$('a').click(function() {
    $doc.animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 35
    }, 550);
    return false;
});

function getTimeRemaining(endtime) {
    // var countDownDate = new Date("April 28, 2018 10:00:00").getTime();

    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        var t = getTimeRemaining("April 28, 2018 10:00:00");

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}

var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
initializeClock('clockdiv', deadline);


var modal = document.getElementById('myModal');
var modal_text = document.getElementById('modalText');

var btn = document.getElementById("btnPalestrantes");

var span = document.getElementsByClassName("close")[0];

function openModal(text) {
    modal_text.innerHTML = text;
    modal.style.display = "block";
    $('body')[0].style.overflow = "hidden";
}

span.onclick = function() {
    modal.style.display = "none";
    $('body')[0].style.overflow = "auto";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        $('body')[0].style.overflow = "auto";
    }
}
